# VCSELArrayAnalysis

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://pawelstrzebonski.gitlab.io/VCSELArrayAnalysis.jl)
[![Build Status](https://gitlab.com/pawelstrzebonski/VCSELArrayAnalysis.jl/badges/master/pipeline.svg)](https://gitlab.com/pawelstrzebonski/VCSELArrayAnalysis.jl/pipelines)
[![Coverage](https://gitlab.com/pawelstrzebonski/VCSELArrayAnalysis.jl/badges/master/coverage.svg)](https://gitlab.com/pawelstrzebonski/VCSELArrayAnalysis.jl/commits/master)

## About

`VCSELArrayAnalysis.jl` is a Julia package for analyzing measurements
of coherent VCSEL arrays and characterizing their coherence using a
variety of measurement data and computational methods.

## Features

* Far-field visibility calculation
* Far-field Fourier analysis
* Far-field beam-steering analysis
* Current-power modeling and power enhancement calculation
* Spectral mode intersection modeling and prediction
* Voltage derivative (differential resistance) calculation

Other relevant functionality that depends on artificial neural
networks is provided by the sister package
[VCSELArrayAnalysisML.jl](https://gitlab.com/pawelstrzebonski/VCSELArrayAnalysisML.jl).

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/VCSELArrayAnalysis.jl
```

## Documentation

Package documentation and usage examples can be found at the
[Gitlab Pages for VCSELArrayAnalysis.jl](https://pawelstrzebonski.gitlab.io/VCSELArrayAnalysis.jl/).
