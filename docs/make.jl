using Documenter
import VCSELArrayAnalysis

makedocs(
    sitename = "VCSELArrayAnalysis.jl",
    repo = Documenter.Remotes.GitLab("pawelstrzebonski", "VCSELArrayAnalysis.jl"),
    pages = [
        "Home" => "index.md",
        "Examples" => "example.md",
        "References" => "references.md",
        "Contributing" => "contributing.md",
        "src/" => [
            "farfield_beamsteering.jl" => "farfield_beamsteering.md",
            "farfield_fft.jl" => "farfield_fft.md",
            "farfield_visibility.jl" => "farfield_visibility.md",
            "linear_modeling.jl" => "linear_modeling.md",
            "power.jl" => "power.md",
            "spectral_predictor.jl" => "spectral_predictor.md",
            "utils.jl" => "utils.md",
            "voltage.jl" => "voltage.md",
        ],
        "test/" => [
            "farfield_beamsteering.jl" => "farfield_beamsteering_test.md",
            "farfield_fft.jl" => "farfield_fft_test.md",
            "farfield_visibility.jl" => "farfield_visibility_test.md",
            "linear_modeling.jl" => "linear_modeling_test.md",
            "power.jl" => "power_test.md",
            "spectral_predictor.jl" => "spectral_predictor_test.md",
            "utils.jl" => "utils_test.md",
            "voltage.jl" => "voltage_test.md",
        ],
    ],
)
