# spectral_predictor.jl

## Description

This file implements spectral mode identification and their linear modeling
for the purpose of predicting where these modes intersect (potential coherent
regions).

## Functions

```@autodocs
Modules = [VCSELArrayAnalysis]
Pages   = ["spectral_predictor.jl"]
```
