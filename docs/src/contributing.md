# Contributing and Development

Contributions to this package are welcomed. Please submit issues/pull-requests
on the GitLab project.

## TODOs

The following are some areas for development and improvement:

* More, better unit tests and documentation
* Better examples of usage
* Optimizations
* Generalization of functions to higher dimensions (eg more than 2 array elements and currents, 2D far-field images)
* Better current-power modeling/regression

## Guidance

When adding functionality or making modifications, please keep in mind
the following:

* We aim to support arrays with arbitrary numbers of laser elements
* We aim to support analysis of 1D and 2D beam images
* We aim to minimize the amount of code and function repetition
