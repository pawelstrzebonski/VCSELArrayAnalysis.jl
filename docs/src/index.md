# VCSELArrayAnalysis.jl Documentation

## About

`VCSELArrayAnalysis.jl` is a Julia package for analyzing measurements
of coherent VCSEL arrays and characterizing their coherence using a
variety of measurement data and computational methods.

## Installation

This package is not in the official Julia package repository, so to
install this package run the following command in the Julia REPL:

```Julia
]add https://gitlab.com/pawelstrzebonski/VCSELArrayAnalysis.jl
```

Note: You may need to manually install some of the dependencies that
are not in the official package repository (see **Related Packages** below).

## Features

* Far-field visibility calculation
* Far-field Fourier analysis
* Far-field beam-steering analysis
* Current-power modeling and power enhancement calculation
* Spectral mode intersection modeling and prediction
* Voltage derivative (differential resistance) calculation

Note: Many aspects of this package are still work-in-progress, in
particular increasing support for arrays of arbitrary number of elements
and 2D far-field images. However, most analyses should be functional for
2-element VCSEL arrays.

## Related Packages

* [FindPlanes.jl](https://gitlab.com/pawelstrzebonski/FindPlanes.jl) is a dependency
* [FindPeaks.jl](https://gitlab.com/pawelstrzebonski/FindPeaks.jl) is a dependency
* [VCSELArrayAnalysisML.jl](https://gitlab.com/pawelstrzebonski/VCSELArrayAnalysisML.jl) provides other functionality relevant to characterizing coherent laser arrays using machine learning methods
