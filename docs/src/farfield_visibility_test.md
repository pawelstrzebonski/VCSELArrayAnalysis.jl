# farfield_visibility.jl

## Description

Basic unit testing to verify far-field visibility calculations return
reasonable results.
