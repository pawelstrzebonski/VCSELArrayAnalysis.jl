# linear_modeling.jl

## Description

This file implements various linear algebra functions, primarily intended
for the modeling of spectral mode evolution (as hyperplanes in
N-D current-wavelength space) and determining the intersections of these
linear models for the spectral modes.

## Functions

```@autodocs
Modules = [VCSELArrayAnalysis]
Pages   = ["linear_modeling.jl"]
```
