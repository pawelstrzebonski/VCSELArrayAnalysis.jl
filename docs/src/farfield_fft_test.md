# farfield_fft.jl

## Description

Basic unit testing to verify far-field Fourier analysis calculations
don't return errors and return reasonable results.
