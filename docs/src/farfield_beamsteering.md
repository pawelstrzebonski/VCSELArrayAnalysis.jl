# farfield_beamsteering.jl

## Description

This file implements beam-steering analysis of far-field images, assuming
that the laser array acts as a phased array of point-source emitters
(analogously to phased antenna arrays).

## Functions

```@autodocs
Modules = [VCSELArrayAnalysis]
Pages   = ["farfield_beamsteering.jl"]
```
