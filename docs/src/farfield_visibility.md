# farfield_visibility.jl

## Description

This file implements the calculation of far-field visibility (analyzing
the far-field interference fringe minima and maxima).

## Functions

```@autodocs
Modules = [VCSELArrayAnalysis]
Pages   = ["farfield_visibility.jl"]
```
