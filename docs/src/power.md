# power.jl

## Description

This file implements the modeling of current-power relations and the
prediction of uncoupled array power and coherent power enhancement
using those models. Also implements image analysis (correlation to a
"ridge" features) of power measurements.

## Functions

```@autodocs
Modules = [VCSELArrayAnalysis]
Pages   = ["power.jl"]
```
