# Example

## Far-field Analysis

### 1D Images

Let's consider a 1D far-field measurement, `ff1d`:

```Julia
# Load the far-field image as a vector (specific to your process flow)
ff1d = loadmeasurement()
```

There are a couple ways of analyzing this far-field. First, we can
calculate the far-field visibility parameter (a measure of the prominence
of far-field interference fringes calculated using minima and maxima,
indicative of coherent coupling in certain coherent laser arrays):

```Julia
# Run the visibility analysis
vis = VCSELArrayAnalysis.farfieldvisibility_analysis(ff1d)
# vis should be a value between 0 (uncoupled cavities) and 1 (coupled cavities)
```

Due to the use of maxima and minima in calculating the visibility parameter,
the algorithm may have issues with measurement noise. If there are issues,
you can try to mitigate them by adjusting the noise filtering and minima/maxima
finding options of the `farfieldvisibility_analysis` function.

A potentially more resilient way of analyzing the far-field is using
Fourier methods:

```Julia
# Run the Fourier analysis
power, ratio, phase = VCSELArrayAnalysis.farfieldfftpeak_analysis(ff1d)
```

This analysis returns `power`, the sum of the intensities, `ratio`, an
measure of the far-field interference fringe (somewhat similar to the
visibility parameter, but whose maximal value indicating coupled cavities
varies depending on the theoretical "ideal" coupled cavity far-field),
and `phase`, a parameter relating the phase of the Fourier peak associated
with the interference fringes (thought to be related to the beam-steering
angle).

If we want to analyze the beam-steering of a far-field and the inter-cavity
phase, we can use the `beamsteering_analysis` function, which analyzes
the system assuming it is analogous to a phased array in antenna theory.
Now, this analysis requires a bit more information to properly analyze
the beam-steering:

```Julia
# Load the angles (in radians) and intensity values for a far-field measurement (specific to your process flow)
angles, ff1d = loadmeasurement()
# Define the spacing between array elements and lasing wavelength
spacing, wavelength = ...
# Run the beam-steering analysis
angle, phase = VCSELArrayAnalysis.beamsteering_analysis(angles, ff1d, spacing, wavelength)
```

This returns the far-field beam-steering `angle` (angle of the intensity
peak), as well as a `phase` parameter (estimated field phase difference
between adjacent array elements).

### 2D Images

The far-field analysis methods have not all be implemented for 2D beam
measurements. As of writing, only the Fourier method
`farfieldfftpeak_analysis` has been implemented and it's use is the same
for 2D as for 1D (except that the input is now a 2D matrix rather than a
1D vector of values).

## Optical Power Analysis

Coherent coupling in VCSEL arrays leads to a formation of array supermodes,
and these supermodes can have lower threshold currents (or rather current
densities) than any of the individual element laser modes. This leads to
a coherent power enhancement effect. In current-power scans of VCSEL
arrays, this tends to show "coherent ridge" features where there are
regions of higher optical power along the diagonal of a current scan
(generally around the regions where the currents to the individual
cavities are roughly equal). The strength of this coherent power enhancement
can be used to analyze the (complex) coupling coefficient of these arrays.
First, we start by loading the relevant current-power data:

```Julia
# Load the current (i1, i2, ...) and power data as vectors (specific to your process flow)
i1, i2, ..., power = loadmeasurement()
# Convert current data to matrix form
currents = [i1 i2 ..]'
```

One way of analyzing the power measurements is based on looking for these
coherent ridge features using an image analysis approach. We calculate
the correlation between the power measurement and a ridge template
to calculate how ridge-like each point in the measurement is (which we
presume to imply how coherent-like each point is). This can be done
using the `ridge_correlation` function:

Note: *As of writing, this analysis only supports 2 element scans sampled
on a rectangular grid*

```Julia
# Define a ridge "width" (in terms of current units)
width = ...
# Run the power ridge correlation analysis
i1, i2, rc = VCSELArrayAnalysis.ridge_correlation(currents, power, width)
```

This returns current axes vectors `i1` and `i2`, as well as the ridge
correlation `rc`, whose magnitude indicates similarity to a ridge-like
feature (oriented along the increasing-currents diagonal) and whose sign
indicates a power enhancement (+) or suppression (-) ridge.

An alternative analysis, one that can help us analyze the strength
of the coherent power enhancement is based on modeling the current-power
relation. If we can predict the uncoupled array power, then we can use
that to estimate the power enhancement. There are two main ways of doing
so. The first is the "naive" approach, `naivepower_analysis`, that assumes
that the total incoherent array power is the sum of the individual element
powers (taken at low currents):

Note: *As of writing, this analysis only supports scans sampled
on a rectangular grid*

```Julia
# Run the "naive" power modeling analysis
axes, power, naivepower, excesspower = VCSELArrayAnalysis.naivepower_analysis(currents, power)
```

It returns a list of current vector `axes` that represent the axes of an
N-D current-power scan, arrays of power values for the measured `power`,
estimated uncoupled array `naivepower`, and estimated coherent power
enhancement `excesspower`.

Now, the naive method does not account for thermal shifting effects, so
a better method is to use a more complex model that can incorporate such
effects. This is provided by the `analyticpower_analysis` function:

```Julia
# Run an analytical power modeling analysis
power, analyticpower, excesspower, powerfun = VCSELArrayAnalysis.analyticpower_analysis(currents, power)
```

Similar to the naive approach, it returns measured power, estimated uncoupled
power, and estimated power enhancement (but as lists of values at the input
current points), and it also returns a function `powerfun` that can be used
to apply the model to a new set of current values to estimate the uncoupled
power.

By default, `analyticpower_analysis` tries to model the current-power as
a summation of rectified linear functions of shift-adjusted element
currents. This can be modified to use higher order polynomial via the
`order` option or splines by setting `splines` to `true` (the number of
knots per spline is defined by `knots`). By default the function will
try a best fit to the overall dataset (coupled and uncoupled points), which
may not be optimal. The `overestimationloss` option can punish the model
for estimating too high of a power, potentially improving the estimation
of uncoupled power (minimizing error due to the coupled points). Various
other options can be set to control the model optimization routine.

## Optical Spectrum Analysis

Coherent coupling between cavities tends to happen when the cavity modes
are aligned in spectral space producing a single spectral peak. This
means we can analyze coherence by counting the number of spectral peaks
as a function of driving currents. Perhaps more importantly, this means
that if we can model the relation between driving current and spectral
peak wavelengths then we can predict where the peaks will align and
at what driving currents we may expect coherent coupling (even if none
of our original spectral measurements happened upon an coherently coupled
operating point).

To start our spectrum analysis is load and process the spectral measurements
to find the spectral peaks:

```Julia
# Load a list of current points cs,
# list of wavelength vectors ws
# and list of spectral power vectors ps as vectors (specific to your process flow)
cs, ws, ps = loadmeasurement()
# Convert data to matrix form
currents, wavelengths, powers = reduce(hcat, cs), reduce(hcat, ws), reduce(hcat, ps)
# Find the spectral peaks
modepoints = VCSELArrayAnalysis.findmodepoints(currents, wavelengths, powers)
```

Now, `modepoints` is a matrix of current-wavelength information representing
all of the relevant peaks found in the optical spectra. Now, `findmodepoints`
has it's own default peak-finding algorithm but it is very likely that
it needs tuning for your data, so you need to pass a peak-finding function
as `findpeaksfun`. You can use any function that takes a list of values
and returns the indices of the peaks, but you may want to just use the
`VCSELArrayAnalysis.FindPeaks.findpeaks` (a different package that is a
dependency of this package) with appropriate options set):

```Julia
# Define a properly tuned peak-finding function (adjust the settings as appropriate)
findpeaksfun = wavelengths -> VCSELArrayAnalysis.FindPeaks.findpeaks(wavelengths, height = -70, prominence = 3, ...)
# Find the spectral peaks using our customized function
modepoints = VCSELArrayAnalysis.findmodepoints(currents, wavelengths, powers, findpeaksfun = findpeaksfun)
```

Now, once you have found the spectral peaks as a function of driving currents,
you can use `spectral_predictor` to use this data to predict where we
may have coherent coupling:

```Julia
# Model and predict coherent driving currents
predicted_coherence = VCSELArrayAnalysis.spectral_predictor(modepoints)
```

Now `predicted_coherence` will be a matrix of current-wavelength points
which may exhibit some degree of coherent coupling. Needless to say,
this spectral modeling is rather involved and getting good results will
likely involve adjusting the options to `spectral_predictor` to get good
results.

## Voltage Analysis

When scanning the driving currents to an array, the differential resistance
may change when entering or exiting coherent coupling (either increased
or decreased differential resistance). The differential
resistance can be calculated as the derivative of the voltage, and this
is implemented via the `voltage_analysis` function:

Note: *As of writing, this analysis only supports 2 element scans sampled
on a rectangular grid*

```Julia
# Load the current (i1, i2, ...) and voltage data as vectors (specific to your process flow)
i1, i2, ..., voltage = loadmeasurement()
# Convert current data to matrix form
currents = [i1 i2 ..]'
# Calculate the differential resistance given a voltage across a single cavity
i1, i2, dvdi1, dvdi2 = VCSELArrayAnalysis.voltage_analysis(currents, voltage)
```

This returns the current axes `i1` and `i2`, as well as the partial
derivatives versus each of the driving currents, `dvdi1` and `dvdi2`.
In our experience the voltage measurements tend to be noisy, in which
case the measurement may need to be smoothed (which can be accomplished
via some of the `voltage_analysis` options).
