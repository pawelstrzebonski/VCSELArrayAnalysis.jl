# farfield_fft.jl

## Description

This file implements the analysis of far-field coherent fringes using
Fourier methods.

## Functions

```@autodocs
Modules = [VCSELArrayAnalysis]
Pages   = ["farfield_fft.jl"]
```
