@testset "farfield_fft.jl" begin
    res = 0

    #TODO: test result values
    ff = [exp(-abs2(x - 1)) + exp(-abs2(x + 1)) for x = -4:0.1:4]
    @test (res = VCSELArrayAnalysis.farfieldfftpeak_analysis(ff, N = 0); true)
    @test length(res) == 4
    @test res[1] >= 0 && 1 >= res[2] >= 0 && -pi <= res[3] <= pi
    @test (res = VCSELArrayAnalysis.farfieldfftpeak_analysis(ff, N = 10); true)
    ff = [
        exp(-(abs2(x - 1) + abs2(y - 1))) + exp(-(abs2(x + 1) + abs2(y + 1))) for
        x = -4:0.1:4, y = -4:0.1:4
    ]
    @test (res = VCSELArrayAnalysis.farfieldfftpeak_analysis(ff, N = 0); true)
    @test length(res) == 4
    @test res[1] >= 0 && 1 >= res[2] >= 0 && -pi <= res[3] <= pi
    @test length(res[4]) == 2
    @test (res = VCSELArrayAnalysis.farfieldfftpeak_analysis(ff, N = 10); true)
end
