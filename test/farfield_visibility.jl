@testset "farfield_visibility.jl" begin
    res = 0

    x = -5:0.1:5
    ff = @. exp(-abs2(x))
    @test (
        res = VCSELArrayAnalysis.farfieldvisibility_analysis(
            ff,
            w = 1,
            N = 1,
            distance = 0.01,
        );
        true
    )
    @test iszero(res)
    ff2 = @. exp(-abs2(x - 1)) + exp(-abs2(x + 1))
    @test (
        res = VCSELArrayAnalysis.farfieldvisibility_analysis(
            ff2,
            w = 1,
            N = 1,
            distance = 0.01,
        );
        true
    )
    @test 0.2 > res > 0.15
    ff3 = @. exp(-abs2(x - 2)) + exp(-abs2(x + 2))
    @test (
        res = VCSELArrayAnalysis.farfieldvisibility_analysis(
            ff3,
            w = 1,
            N = 1,
            distance = 0.01,
        );
        true
    )
    @test 0.95 > res > 0.9
    ffs = [ff ff2 ff3]
    @test (
        res = VCSELArrayAnalysis.farfieldvisibility_analysis(
            ffs,
            w = 1,
            N = 1,
            distance = 0.01,
        );
        true
    )
    @test iszero(res[1]) && 0.2 > res[2] > 0.15 && 0.95 > res[3] > 0.9

    # Test to see if stuff doesn't fail
    @test (
        res = VCSELArrayAnalysis.farfieldvisibility_analysis(
            ff,
            w = 1,
            N = 1,
            distance = 0.01,
            ave = true,
            env = true,
        );
        true
    )
end
