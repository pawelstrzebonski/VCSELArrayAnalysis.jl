@testset "utils.jl" begin
    @test VCSELArrayAnalysis.relu(1) == 1
    @test VCSELArrayAnalysis.relu(-1) == 0
    @test VCSELArrayAnalysis.relu([-1, 0, 1]) == [0, 0, 1]

    @test VCSELArrayAnalysis.mean(1:3) == 2

    @test VCSELArrayAnalysis.mse(ones(3), ones(3)) == 0
    @test VCSELArrayAnalysis.mse(ones(3), zeros(3)) == 1

    p = reduce(hcat, [[x, y, x + y] for x = 1:3, y = 1:5])
    p, v = p[1:2, :], p[3, :]
    axes, values = VCSELArrayAnalysis.list2array(p, v)
    @test length.(axes) == [3, 5]
    @test size(values) == (3, 5)
    @test !any(isnan.(values))
    axes, values = VCSELArrayAnalysis.list2array(p[:, 2:end], v[2:end])
    @test any(isnan.(values))
    axes, values = VCSELArrayAnalysis.list2array(p, [v v]')
    @test size(values) == (3, 5, 2)
    @test !any(isnan.(values))

    #TODO: better test of results
    p = reduce(hcat, [[x, y, x + y] for x = 1:3, y = 1:5])
    p, v = p[1:2, :], p[3, :]
    axes, values = VCSELArrayAnalysis.list2array(p, v)
    dzdx, dzdy = VCSELArrayAnalysis.grad(axes[1], axes[2], values)
    @test size(dzdx) == size(values) .- (1, 0)
    @test size(dzdy) == size(values) .- (0, 1)
    @test all(isone.(dzdx))
    @test all(isone.(dzdy))

    @test VCSELArrayAnalysis.zpad(ones(3), 3) == [0, 0, 0, 1, 1, 1, 0, 0, 0]
    @test VCSELArrayAnalysis.zpad(ones(3), 1) == [0, 1, 1, 1, 0]
    @test VCSELArrayAnalysis.zpad(ones(1, 2), 1) == [0 0 0 0; 0 1 1 0; 0 0 0 0]

    @test VCSELArrayAnalysis.epad([1, 2], 1) == [1, 1, 2, 2]
    @test VCSELArrayAnalysis.epad([1 2], 1) == [1 1 2 2; 1 1 2 2; 1 1 2 2]
    @test VCSELArrayAnalysis.epad([1 2], 1, dims = (2)) == [1 1 2 2]

    #TODO: better test of results
    @test VCSELArrayAnalysis.gaussiansmooth(ones(3), 1, N = 1) == ones(3)
    @test VCSELArrayAnalysis.gaussiansmooth([1, 2, 3], 0, N = 1) == [1, 2, 3]
end
