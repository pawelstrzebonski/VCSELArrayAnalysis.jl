@testset "farfield_beamsteering.jl" begin
    res = 0

    x = -1:0.01:1
    ff = @. exp(-abs2(x - 0.1))
    @test (res = VCSELArrayAnalysis.beamsteering_angle(x, ff); true)
    @test res == 0.1
    ff = @. exp(-abs2(x + 0.1))
    @test (res = VCSELArrayAnalysis.beamsteering_angle(x, ff); true)
    @test res == -0.1
    ff = [exp.(-abs2.(x .+ 0.1)) exp.(-abs2.(x .- 0.1))]
    @test (res = VCSELArrayAnalysis.beamsteering_angle(x, ff); true)
    @test res == [-0.1 0.1]

    @test approxeq(VCSELArrayAnalysis.phasedifference(1, 1, 1), 5.287)

    @test VCSELArrayAnalysis.phasenorm(1) == 1
    @test approxeq(VCSELArrayAnalysis.phasenorm(1 + 2 * pi), 1)
    @test approxeq(VCSELArrayAnalysis.phasenorm(1 + pi), 1 - pi)

    ff = @. exp(-abs2(x - 0.1))
    @test (res = VCSELArrayAnalysis.beamsteering_analysis(x, ff, 1, 1); true)
    @test res[1] == 0.1
    @test -pi <= res[2] <= pi
    ff = [exp.(-abs2.(x .+ 0.1)) exp.(-abs2.(x .- 0.1))]
    @test (res = VCSELArrayAnalysis.beamsteering_analysis(x, ff, 1, 1); true)
    @test res[1] == [-0.1, 0.1]
    @test all(-pi .<= res[2] .<= pi)
    @test length(res[1]) == length(res[2]) == 2
end
