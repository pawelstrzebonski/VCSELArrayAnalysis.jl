@testset "linear_modeling.jl" begin
    res = 0
    @test (res = VCSELArrayAnalysis.proj([1, 2, 3], [1, 0, 1]); true)
    @test isapprox(res, sqrt(8))

    @test (res = VCSELArrayAnalysis.orth([1 2 0; 2 1 0]); true)
    @test iszero(res[:, 1]' * res[:, 2]) &&
          isone(sum(abs2.(res[:, 1]))) &&
          isone(sum(abs2.(res[:, 2])))

    ps = rand(3, 10) .* 3
    @test (res = VCSELArrayAnalysis.normalizepoints(ps); true)
    @test size(res[1]) == (3, 10) && length(res[2]) == 3
    @test all(@. 0 <= res[1] <= 1)
    @test (res = VCSELArrayAnalysis.unnormalizepoints(res...); true)
    @test all(isapprox.(ps, res))

    normals = [[1, 1, 1], [1, 1, -1]]
    distances = [1, 1]
    @test (res = VCSELArrayAnalysis.findintersection(normals, distances); true)
    @test isapprox(res[1], [0.5, 0.5, 0])
    @test isapprox(res[2][:], [-1, 1, 0] ./ sqrt(2))
    @test (res = VCSELArrayAnalysis.affinedistance(res..., [0, 0, 0]); true)
    @test isapprox(res, 1 / sqrt(2))
end
