@testset "voltage.jl" begin
    currents = reduce(hcat, [[i, j] for i = 1:10, j = 1:10][:])
    voltage = [1 + i / 2 for i = 1:10, j = 1:10][:]

    res = 0
    @test (res = VCSELArrayAnalysis.voltage_analysis(currents, voltage); true)
    @test res[1] == res[2] == 1:10
    @test size(res[3]) == (9, 10) && size(res[4]) == (10, 9)
    @test all(res[3] .== 0.5)
    @test all(res[4] .== 0)
end
