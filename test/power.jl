@testset "power.jl" begin
    #TODO: Test higher/lower dimensions?

    res = 0
    @test (res = VCSELArrayAnalysis.gaussian_ridge_matrix(1, 1); true)
    @test size(res) == (3, 3)
    @test isapprox(sum(res), 0, atol = 1e-10)
    @test isapprox(sum(abs2.(res)), 1)
    @test (res = VCSELArrayAnalysis.gaussian_ridge_matrix(2, 3); true)
    @test size(res) == (5, 5)
    @test isapprox(sum(res), 0, atol = 1e-10)
    @test isapprox(sum(abs2.(res)), 1)

    #TODO: test result values
    @test (res = VCSELArrayAnalysis.ridge_correlation(rand(10, 10), 3, 1); true)
    @test size(res) == (10, 10)

    #TODO: test result values
    p = reduce(hcat, [[x, y, x + y] for x = 1:3.0, y = 1:5.0])
    p, v = p[1:2, :], p[3, :]
    @test (res = VCSELArrayAnalysis.powerridge_analysis(p, v, 1); true)
    @test length(res[1]) == 3
    @test length(res[2]) == 5
    @test size(res[3]) == (3, 5)

    #TODO: test result values
    @test (res = VCSELArrayAnalysis.naivepower_analysis(p, v); true)
    @test length(res[1][1]) == 3
    @test length(res[1][2]) == 5
    @test size(res[2]) == size(res[3]) == size(res[4]) == (3, 5)

    #TODO: test result values
    @test (res = VCSELArrayAnalysis.analyticpower_analysis(p, v, order = 1); true)
    @test length(res[1]) == length(res[2]) == length(res[3]) == length(v)
    @test length(res[4](p)) == length(v)
    @test (res = VCSELArrayAnalysis.analyticpower_analysis(p, v, order = 2); true)
    @test length(res[1]) == length(res[2]) == length(res[3]) == length(v)
    @test length(res[4](p)) == length(v)
    @test (
        res = VCSELArrayAnalysis.analyticpower_analysis(p, v, order = 2, splines = true); true
    )
    @test length(res[1]) == length(res[2]) == length(res[3]) == length(v)
    @test length(res[4](p)) == length(v)
    @test (
        res = VCSELArrayAnalysis.analyticpower_analysis(
            p,
            v,
            order = 1,
            filtercoherent = true,
        );
        true
    )
    @test length(res[1]) == length(res[2]) == length(res[3]) == length(v)
    @test length(res[4](p)) == length(v)
    @test (
        res = VCSELArrayAnalysis.analyticpower_analysis(
            p,
            v,
            order = 1,
            filterthreshold = true,
            threshold = 2,
        );
        true
    )
    @test length(res[1]) == length(res[2]) == length(res[3]) == length(v)
    @test length(res[4](p)) == length(v)
end
