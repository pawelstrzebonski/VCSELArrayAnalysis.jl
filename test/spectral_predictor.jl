@testset "spectral_predictor.jl" begin
    w = 0:0.1:10
    p = @. exp(-(w - 5)^2)
    res = 0
    @test (
        res = VCSELArrayAnalysis.findmodes(w, p, VCSELArrayAnalysis.FindPeaks.findpeaks); true
    )
    @test res == [5]

    #TODO: Validate reasonable peak locations
    ws = reduce(hcat, fill(w, 100))
    ps = reduce(
        hcat,
        [
            (@. exp(-(w - wc)^2) + exp(-(w - wc2)^2)) for wc in LinRange(2, 8, 10) for
            wc2 in LinRange(2, 8, 10)
        ][:],
    )
    cs = reduce(hcat, [[i, j] for i = 1:10, j = 1:10][:])
    @test (res = VCSELArrayAnalysis.findmodepoints(cs, ws, ps); true)
    @test size(res, 1) == 3

    #TODO: Validate reasonable intersection locations
    mp = res
    @test (
        res = VCSELArrayAnalysis.spectral_predictor(
            mp,
            maxiter = 100,
            Ngrid = 50,
            maxdistintersection = 2e-2,
        );
        true
    )
    @test size(res, 1) == 3

end
