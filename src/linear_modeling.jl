import LinearAlgebra

"""
    findintersection(normals::AbstractVector, distances::AbstractVector)->(p0, vecs)

Given a set of hyperplanes defined by normal vectors `normals` and `distances`,
find the intersection as a affine space defined by a point `p0` and vectors
`vecs`.
"""
function findintersection(normals::AbstractVector, distances::AbstractVector)
    M = reduce(hcat, normals)'
    # Find a point in all modes
    p0 = M \ distances
    # Find vectors that are within all modes
    vs = LinearAlgebra.nullspace(M)'
    #TODO: is there a better representation
    p0, vs
end

"""
    proj(x::AbstractVector, n::AbstractVector)

Vector projection of `x` onto `n`.
"""
proj(x::AbstractVector, n::AbstractVector) = LinearAlgebra.dot(x, n) / LinearAlgebra.norm(n)

"""
    orth(M::AbstractMatrix)

Create ortho-normal basis matrix for the space spanned by `M`.
"""
orth(M::AbstractMatrix) = LinearAlgebra.nullspace(LinearAlgebra.nullspace(M)')

"""
    normalizepoints(points::AbstractMatrix)->(points, offset, scale)

Transforms points into a new system by subtracting an `offset` and scaling
by `1/scale` to get a set of points within a `[0,1]` hypercube.
"""
function normalizepoints(points::AbstractMatrix)
    np = copy(points)
    offset = minimum.([x for x in eachrow(points)])
    scale = maximum.([x for x in eachrow(points)]) .- offset
    np .-= offset
    np ./= scale
    np, offset, scale
end

"""
    unnormalizepoints(
		points::AbstractMatrix,
		offset::AbstractVector,
		scale::AbstractVector,
	)

Undo the normalizing transformation done by `normalizepoints`.
"""
function unnormalizepoints(
    points::AbstractMatrix,
    offset::AbstractVector,
    scale::AbstractVector,
)
    unp = copy(points)
    unp .*= scale
    unp .+= offset
    unp
end

"""
    affinedistance(p0::AbstractVector, vecs::AbstractMatrix, x::AbstractVector)

Find the distance from `x` to the affine space defined by a point `p0`
and vectors `vecs`.
"""
function affinedistance(p0::AbstractVector, vecs::AbstractMatrix, x::AbstractVector)
    @assert length(p0) == size(vecs, 2)
    vecsorth = orth(vecs)'
    # Find the projection of x onto the affine space
    p = sum([proj(x - p0, n) * n / LinearAlgebra.norm(n) for n in eachrow(vecsorth)])
    # Now the rejection
    r = (x - p0) - p
    # And the distance is the norm of the rejection
    LinearAlgebra.norm(r)
end
