import FindPeaks, FindPlanes

"""
    findmodes(
		wavelength::AbstractVector,
		spectrum::AbstractVector,
		findpeaksfun::Function,
	)

Find the wavelengths of the peaks using `findpeaksfun` that returns
indices of peaks in `spectrum`.
"""
function findmodes(
    wavelength::AbstractVector,
    spectrum::AbstractVector,
    findpeaksfun::Function,
)
    idx = findpeaksfun(spectrum)
    wavelength[idx]
end

"""
    findmodepoints(
		currents::AbstractMatrix,
		wavelengths::AbstractMatrix,
		powers::AbstractMatrix,
		findpeaksfun::Function = (x -> FindPeaks.findpeaks(x)),
	)

Finds all modal peaks in a set of spectra and return a matrix of the
current-wavelength points. Use `findpeaksfun` to find peaks in spectra.
"""
function findmodepoints(
    currents::AbstractMatrix,
    wavelengths::AbstractMatrix,
    powers::AbstractMatrix,
    findpeaksfun::Function = (x -> FindPeaks.findpeaks(x)),
)
    @assert size(wavelengths) == size(powers)
    @assert size(currents, 2) == size(wavelengths, 2)
    # Find the peaks in each spectrum
    peaks = [
        findmodes(w, s, findpeaksfun) for
        (w, s) in zip(eachslice(wavelengths, dims = 2), eachslice(powers, dims = 2))
    ]
    # Pre-allocate output
    modepoints = zeros(size(currents, 1) + 1, sum(length.(peaks)))
    i = 1
    # For each scan...
    for (c, p) in zip(eachslice(currents, dims = 2), peaks)
        # Iterate over each peak...
        for peak in p
            # And add it to the output array
            modepoints[:, i] .= [c; peak]
            i += 1
        end
    end
    modepoints
end

"""
    spectral_predictor(
		modepoints::AbstractMatrix;
		maxmodes::Integer = 0,
		Ngrid::Integer = 100,
		maxdistplanefit::Number = 3e-3,
		maxdistintersection::Number = 3e-3,
		maxiter::Integer = 50,
	)

Predict where there could be coherent coupling given a set of modal points
`modepoints` (returned from `findmodepoints`).

# Arguments:
- `maxmodes::Integer`: number of spectral modes to fit (default to # of currents).
- `Ngrid::Integer`: number of grid-divisions per axis to evaluate as potential intersections.
- `maxdistplanefit::Number`: maximal distance in normalized current-wavelength space to consider points to be part of a mode.
- `maxdistintersection::Number`: maximal distance in normalized current-wavelength space to consider points to be part of an intersection.
- `maxiter::Integer`: maximal number of iterations for RANSAC plane-fitting.
"""
function spectral_predictor(
    modepoints::AbstractMatrix;
    maxmodes::Integer = 0,
    Ngrid::Integer = 100,
    maxdistplanefit::Number = 3e-3,
    maxdistintersection::Number = 3e-3,
    maxiter::Integer = 50,
)
    Nvcsel = size(modepoints, 1) - 1

    # Convert to a normalized space with points bounded within a unit hypercube
    points, offset, scale = normalizepoints(modepoints)

    # Try to find the planes
    maxmodes = iszero(maxmodes) ? Nvcsel : maxmodes
    planes = FindPlanes.multiplanefit(
        points,
        maxplanes = maxmodes,
        maxdist = maxdistplanefit,
        maxiter = maxiter,
    )

    # Find all potential intersections between planes
    @info "Finding intersections between planes."
    intersections = [
        findintersection([planes[i][1], planes[j][1]], [planes[i][2], planes[j][2]]) for
        i = 1:length(planes), j = 1:length(planes) if j > i
    ]
    @info string("Found ", length(intersections), " potential intersections.")

    # Test a range of potential current and wavelength values as potential intersections
    ptrng = LinRange(0, 1, Ngrid)
    pointiter = Iterators.product(fill(ptrng, Nvcsel + 1)...)
    potentialpoints = []
    @info "Identifying intersections within problem space."
    for point in pointiter
        matches = 0
        # For each intersection...
        for intersection in intersections
            # Test if the current point is close to the affine space
            if affinedistance(intersection..., [point...]) < maxdistintersection
                # If so, note that there may be an intersection here
                matches += 1
            end
        end
        # And if 1 or more potential intersections are found, add this point to a list
        if matches > 0
            push!(potentialpoints, [point...])
        end
    end
    @info string("Found ", length(potentialpoints), " potential points.")

    # Translate from normalized space to experiment space
    potentialpoints = reduce(hcat, potentialpoints)
    potentialpoints = unnormalizepoints(potentialpoints, offset, scale)

    potentialpoints
end
