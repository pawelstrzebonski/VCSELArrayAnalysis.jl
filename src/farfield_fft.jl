import FFTW, FindPeaks

"""
    farfieldfftpeak_analysis(farfield; N = 0)->(power, ratio, phase, freq)

Analyze the far-field coherence and shift using FFT methods. Estimates
the far-field `power`, the coherence via the `ratio` of the dominant
interference fringe frequency component to the DC component, the
`phase` of the interference fringe frequency component (related
to the shifting of the fringes), and the spatial frequency `freq`
of the interference fringes.

# Arguments
- `farfield`: a single far-field measurement.
- `N = 0`: the number of elements to zero-pad the far-field (to increase FFT resolution).
"""
function farfieldfftpeak_analysis(farfield::AbstractVector; N::Integer = 0)
    # Estimate total power
    power = sum(farfield)
    # Determine FFT, applying zeropadding to increase frequency resolution
    # Note: rfft will only return frequencies >=0
    Ffarfield = FFTW.rfft(FFTW.ifftshift(zpad(farfield, N)))
    Ffarfieldfreq = FFTW.rfftfreq(length(farfield) + 2 * N)
    # Find peaks in the FFT, zeropad to enable finding peaks on edges of domain
    peaks = FindPeaks.findpeaks(zpad(abs.(Ffarfield), 1))
    # Shift peaks over due to the zeropadding
    peaks = [peak - 1 for peak in peaks]
    # Sort by decreasing peak values
    peakvals, peakfreq = Ffarfield[peaks], Ffarfieldfreq[peaks]
    ord = sortperm(abs.(peakvals), rev = true)
    peaks, peakvals, peakfreq = peaks[ord], peakvals[ord], peakfreq[ord]
    # Find ratio of side-lobe to DC peak and side-lobe phase
    # (assuming highest peak is DC and second-highest is side-lobe)
    if length(peaks) > 1
        ratio = abs(peakvals[2]) / abs(peakvals[1])
        phase = angle(peakvals[2])
        freq = peakfreq[2]
    else
        ratio = zero(abs(peakvals[1]))
        phase = zero(abs(peakvals[1]))
        freq = zero(abs(peakvals[1]))
    end
    power, ratio, phase, freq
end
function farfieldfftpeak_analysis(farfield::AbstractMatrix; N::Integer = 0)
    # Estimate total power
    power = sum(farfield)
    # Determine FFT, applying zeropadding to increase frequency resolution
    # Note: rfft will only return frequencies >=0 on first axis, all on subsequent axes
    Ffarfield = FFTW.rfft(FFTW.ifftshift(zpad(farfield, N)))
    # Determine the frequencies and sort accordingly (zero-freq centering)
    thetax = FFTW.rfftfreq(size(farfield, 1) + 2 * N)
    thetay = FFTW.fftfreq(size(farfield, 2) + 2 * N)
    idx = sortperm(thetay)
    thetay = thetay[idx]
    Ffarfield .= Ffarfield[:, idx]
    # Find peaks in the non-negative FFT
    peaks = FindPeaks.findpeaks(zpad(abs.(Ffarfield), 1))
    # Shift peaks over due to the zeropadding
    peaks = [peak - CartesianIndex(1, 1) for peak in peaks]
    # Sort by decreasing peak values
    peakvals = Ffarfield[peaks]
    ord = sortperm(abs.(peakvals), rev = true)
    peaks, peakvals = peaks[ord], peakvals[ord]
    # Find ratio of side-lobe to DC peak and side-lobe phase
    # (assuming highest peak is DC and second-highest is side-lobe)
    if length(peaks) > 1
        ratio = abs(peakvals[2]) / abs(peakvals[1])
        phase = angle(peakvals[2])
        freq = (thetax[peaks[2][1]], thetay[peaks[2][2]])
    else
        ratio = zero(abs(peakvals[1]))
        phase = zero(abs(peakvals[1]))
        freq = (zero(abs(peakvals[1])), zero(abs(peakvals[1])))
    end
    power, ratio, phase, freq
end
