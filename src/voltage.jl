#TODO: Make N-D?

#TODO: Differential resistance analysis
"""
    voltage_analysis(
		currents::AbstractMatrix,
		voltages::AbstractVector;
		smoothdims = (),
		width::Number = 0.1,
		N::Number = 2,
	)->(i1, i2, dv/di1, dv/di2)

Calculate the differential resistances. Optionally, apply Gaussian smoothing
along dimensions `smoothdims` with for a given `width` and padding radius
of `N` widths.
"""
function voltage_analysis(
    currents::AbstractMatrix,
    voltages::AbstractVector;
    smoothdims = (),
    width::Number = 0.1,
    N::Number = 2,
)
    @assert size(currents, 2) == length(voltages)
    @assert size(currents, 1) == 2

    # Convert to matrices
    (i1, i2), v = list2array(currents, voltages)

    # Determine average current spacing
    di = mean(diff(i1)) / 2 + mean(diff(i2)) / 2
    # Convert current-space width to sample-number width
    w = width / di
    # Determine filter radius
    n = round(Int, N * w)
    # Gaussian smooth along desired dimensions
    for d in smoothdims
        v = reduce(hcat, [gaussiansmooth(v, w, N = n) for v in eachslice(v, dims = d)])
        v = isone(d) ? v' : v
    end
    i1, i2, grad(i1, i2, v)...
end
