import FFTW, Optim, Statistics

"""
    norm2(x::Tuple)

2-Norm of a Tuple.
"""
norm2(x::Tuple) = sum(abs2.(x))

"""
    gaussiansmooth(z0::AbstractArray, w::Number; N::Integer=0)

Gaussian filter smoothing of array `z0` with filter of Gaussian width
`w`. Pad arrays by `N` elements to avoid circular convolution related
edge distortion.
"""
function gaussiansmooth(z0::AbstractArray, w::Number; N::Integer = 0)
    if iszero(w)
        return z0
    end
    # Edge-pad first to allow mitigation of circular-convolution effects
    z = epad(z0, N)
    s = size(z)
    # Find the center of the array
    center = div.(s, 2)
    # Generate a Gaussian function filter array
    filter = [exp.(-norm2(Tuple(p) .- center) / w^2) for p in CartesianIndices(s)]
    filter ./= sum(filter)
    # Convolve by the filter using FFT method
    filtered = FFTW.ifftshift(FFTW.ifft(FFTW.fft(filter) .* FFTW.fft(z)))
    # Return absolute value of the trimmed smoothed array (cutting off the padding)
    abs.(filtered[CartesianIndex(fill(N, ndims(z0))...).+CartesianIndices(size(z0))])
end

"""
    zpad(x, N[; dims = (1, 2)])

Zero-pad `x` with `N` zeros on each edge. For 2D arrays, `dims` can
be used to select dimensions to be padded.
"""
function zpad(x::AbstractVector, N::Integer)
    if iszero(N)
        return x
    end
    x2 = zeros(length(x) + 2 * N)
    x2[N.+(1:length(x))] .= x
    x2
end
function zpad(x::AbstractMatrix, N::Integer; dims = (1, 2))
    if iszero(N)
        return x
    end
    xpad, ypad = (1 in dims ? N : 0), (2 in dims ? N : 0)
    x2 = zeros(size(x) .+ 2 .* (xpad, ypad)...)
    x2[xpad.+(1:size(x, 1)), ypad.+(1:size(x, 2))] .= x
    x2
end

"""
    epad(x, N[; dims = nothing])

Pad `x` with `N` elements on each edge of the original edge values.
`dims` can be used to select dimensions to be padded (`nothing` will
pad all dimensions).
"""
function epad(x::AbstractArray, N::Integer; dims = nothing)
    @assert N >= 0
    # Default to all dimensions
    dims = isnothing(dims) ? (1:ndims(x)) : dims
    # Trivial case of no padding, simply return input
    if iszero(N) || isempty(dims)
        return x
    end
    # Get the size of input
    s = size(x)
    # Generate the ranges on each dimension, extending beyond the bounds
    # if we pad a dimension, otherwise use normal full range
    rngs = [(i in dims ? 1 - N : 1):(i in dims ? s[i] + N : s[i]) for i = 1:ndims(x)]
    # Index the original array, clamping the out-of-bounds indices to the
    # edge indices
    x[[clamp.(r, 1, s[i]) for (i, r) in enumerate(rngs)]...]
end

"""
    list2array(points::AbstractMatrix, values)->(axes, values)

Convert a list of points defined by `(points[:, i]..., values[(:, )i])`
into a matrix `values` with axes defined by the vectors in the array
`axes`. Missing points will be assigned `NaN` value.
"""
function list2array(points::AbstractMatrix, values::AbstractVector)
    @assert size(points, 2) == length(values)
    axes = [sort(unique(p)) for p in eachslice(points, dims = 1)]
    out = zeros(length.(axes)...) .* NaN
    for (i, v) in enumerate(values)
        idxs = findfirst.(isequal.(points[:, i]), axes)
        out[idxs...] = v
    end
    axes, out
end
function list2array(points::AbstractMatrix, values::AbstractMatrix)
    @assert size(points, 2) == size(values, 2)
    axes = [sort(unique(p)) for p in eachslice(points, dims = 1)]
    out = zeros(length.(axes)..., size(values, 1)) .* NaN
    for (i, v) in enumerate(eachslice(values, dims = 2))
        idxs = findfirst.(isequal.(points[:, i]), axes)
        out[idxs..., :] .= v
    end
    axes, out
end

mean = Statistics.mean

"""
    mse(x::AbstractArray, y::AbstractArray)

Mean-Square-Error.
"""
mse(x::AbstractArray, y::AbstractArray) = mean(@. (x - y)^2)

#TODO: Make this N-D?
"""
    grad(x::AbstractVector, y::AbstractVector, z::AbstractMatrix)->(dzdx, dzdy)

Gradient for `z = f(x, y)`.
"""
function grad(x::AbstractVector, y::AbstractVector, z::AbstractMatrix)
    @assert (length(x), length(y)) == size(z)
    dx = diff(x) * ones(length(y))'
    dy = ones(length(x)) * diff(y)'
    diff(z, dims = 1) ./ dx, diff(z, dims = 2) ./ dy
end

"""
    relu(x)

Rectified linear unit.
"""
relu(x::Number) = x * (x > 0)
relu(x::AbstractArray) = @. x * (x > 0)

"""
    mse(a, b) 

Mean-square-error.
"""
mse(a, b) = Statistics.mean(@. (a - b)^2)

"""
    gaussian_fit([x::AbstractVector, ]y::AbstractVector)->(A, mu, sigma, y2)

Fit a Gaussian function (amplitude `A`, mean `mu`, and deviation `sigma`) to `y`.
`y2` is the best-fit Gaussian function. If grid `x` is not provided, will default to
`1:length(y)`.
"""
function gaussian_fit(x::AbstractVector, y::AbstractVector)
    # Gaussian function
    gf((A, mu, sigma)) = (@. A * exp(-(x - mu)^2 / sigma^2))
    # Define loss using MSE between fitted Gaussian and input
    loss((A, mu, sigma)) = mse(y, gf((A, mu, sigma)))
    # Guess initial Gaussian to center at maximum with 1/4-input-length deviation
    A0 = maximum(abs.(y))
    mu0 = argmax(abs.(y))
    sigma0 = length(y) / 4
    # Find parameters that give best fit
    res = Optim.minimizer(Optim.optimize(loss, [A0, mu0, sigma0]))
    # Return the mean, deviation, and fitted Gaussian
    res..., gf(res)
end
function gaussian_fit(y::AbstractVector)
    x = 1:length(y)
    gaussian_fit(x, y)
end

"""
    gaussian_fit([x::AbstractVector, y::AbstractVector, ]z::AbstractMatrix)->(A, mux, muy, sigmax, sigmay, z2)

Fit a Gaussian function (amplitude `A`, means `mux/muy`, and deviations `sigmax/sigmay`) to `z`.
`z2` is the best-fit Gaussian function. If grids `x/y` is not provided, will default to
`1:size(z,1)` and `1:size(z,2)`.
"""
function gaussian_fit(x::AbstractVector, y::AbstractVector, z::AbstractMatrix)
    # Gaussian function
    gf((A, mux, muy, sigmax, sigmay)) =
        (@. A * exp(-((x - mux)^2 / sigmax^2 + (y - muy)^2 / sigmay^2)))
    # Define loss using MSE between fitted Gaussian and input
    loss((A, mux, muy, sigmax, sigmay)) = mse(z, gf((A, mux, muy, sigmax, sigmay)))
    # Guess initial Gaussian to center at maximum with 1/4-input-length deviation
    A0 = maximum(abs.(z))
    mux0, muy0 = Tuple(argmax(abs.(z)))
    sigmax0, sigmay0 = size(z) ./ 4
    # Find parameters that give best fit
    res = Optim.minimizer(Optim.optimize(loss, [A0, mux0, muy0, sigmax0, sigmay0]))
    # Return the mean, deviation, and fitted Gaussian
    res..., gf(res)
end
function gaussian_fit(z::AbstractMatrix)
    x, y = 1:size(z, 1), 1:size(z, 2)
    gaussian_fit(x, y, z)
end

"""
    maxfilter(x::AbstractVector, N::Integer)

Run a max-filter of window size `N` on `x`.
"""
maxfilter(x::AbstractVector, N::Integer) =
    (xp = epad(x, N); [maximum(xp[i.+(0:(2*N))]) for i = 1:length(x)])
