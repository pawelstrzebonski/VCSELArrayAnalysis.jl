import Optim

#TODO: Make N-D
"""
    gaussian_ridge_matrix(n::Integer, w::Number)

Generate a Gaussian diagonal ridge matrix for filtering of side-length
`2*n+1` and Gaussian width of `w`.
"""
function gaussian_ridge_matrix(n::Integer, w::Number)
    x = LinRange(-1, 1, 1 + n * 2)
    M = [exp(-(x - y)^2 * w) for x in x, y in x]
    M .-= sum(M) / length(M)
    M ./= sqrt(sum(abs2.(M)))
    M
end

#TODO: Make N-D
"""
    ridge_correlation(power::AbstractMatrix, n::Integer, w::Number)

Calculate correlation of `power` to a Gaussian diagonal ridge of width
`w`, using template size of `2*n+1`.
"""
function ridge_correlation(power::AbstractMatrix, n::Integer, w::Number)
    paddedpower = epad(power, n)
    M = gaussian_ridge_matrix(n, w)
    [
        sum(M .* paddedpower[(0:(2*n)).+i, (0:(2*n)).+j]) for i = 1:size(power, 1),
        j = 1:size(power, 2)
    ]
end

#TODO: Make N-D
"""
    function powerridge_analysis(
		currents::AbstractMatrix,
		power::AbstractVector,
		width::Number;
		Nfilter::Integer = 1,
		N::Number = 2,
	)->(i1, i2, cor)

Identify diagonal ridges in the 2D power image using correlation to
a Gaussian `(+1,+1)` diagonal ridge of a given `width`. The size of the
template is related to `N` and the correlation can be repeatedly applied
as specified by `Nfilter`.
"""
function powerridge_analysis(
    currents::AbstractMatrix,
    power::AbstractVector,
    width::Number;
    Nfilter::Integer = 1,
    N::Number = 2,
)
    # Convert power to an array
    (i1, i2), power = list2array(currents, power)
    # Determine average current spacing
    di = mean(diff(i1)) / 2 + mean(diff(i2)) / 2
    # Convert current-space width to sample-number width
    w = width / di
    # Determine filter radius
    n = round(Int, N * w)
    # Apply correlation to the power however many times desired
    rc = power
    for _ = 1:Nfilter
        rc .= ridge_correlation(rc, n, w)
    end
    i1, i2, rc
end

"""
    naive_estimate(power::AbstractArray, idx::CartesianIndex)

Estimates the power at an index as the sum of the corresponding individual
element powers (taken to be at the edges of the array).
"""
function naive_estimate(power::AbstractArray, idx::CartesianIndex)
    o = fill(1, ndims(power) - 1)
    sum([power[circshift([x; o], i - 1)...] for (i, x) in enumerate(Tuple(idx))])
end

"""
    naivepower_analysis(currents::AbstractMatrix, power::AbstractVector)->(currentaxes, power, naivepower, excesspower)

Estimate the uncoupled power "naively" as the sum of individual laser powers,
as derived from the low-current edges. Returns the list of current axes `currentaxes`,
the `power`, estimated uncoupled `naivepower` and coherent power
enhancement `excesspower`. 
"""
function naivepower_analysis(currents::AbstractMatrix, power::AbstractVector)
    # Convert power to an array
    currentaxes, power = list2array(currents, power)
    # For each element in the array, find the naive estimate
    naivepower = [naive_estimate(power, idx) for idx in CartesianIndices(power)]
    excesspower = power - naivepower
    currentaxes, power, naivepower, excesspower
end

"""
    polyfit(
		Ncurrents::Integer,
		Norder::Integer,
		currents::AbstractMatrix,
		coeffs::AbstractVector,
	)

Predict array power using polynomial fit of order `Norder` operating on
`Ncurrents` number of device currents.
"""
function polyfit(
    Ncurrents::Integer,
    Norder::Integer,
    currents::AbstractMatrix,
    coeffs::AbstractVector,
)
    # Create an effective current that is shifted by other currents
    currentsmod = [
        currents[i, :] .+ vec(
            sum(
                currents[1:Ncurrents.!=i, :] .*
                coeffs[((1:(Ncurrents-1)).+(i-1)*(Ncurrents-1))],
                dims = 1,
            ),
        ) for i = 1:Ncurrents
    ]
    coeffoffset = Ncurrents * (Ncurrents - 1)
    # Estimate power as relu of a polynomial function of the shifted current
    powers = [
        relu(
            sum(
                reduce(hcat, ([currentsmod[i] .^ o for o = 0:Norder]))' .* coeffs[coeffoffset.+(1:(Norder+1)).+(i-1)*(Norder+1)],
                dims = 1,
            ),
        ) for i = 1:Ncurrents
    ]
    # Total power is sum of individual powers
    power = vec(sum(powers))
    power
end

# Source: ISLR pg 273

"""
    h(x, z, order)

Truncated power basis of `order` with knot at `z` as function of `x`.
"""
h(x, z, order) = (x > z) * (x - z)^order

"""
    spline(
		currents::AbstractVector,
		Norder::Integer,
		Nknots::Integer,
		coeffs::AbstractVector,
	)

Calculate a spline as a function of `currents` of order `Norder` and
`Nknots` knots using fitting coefficients `coeffs`.
"""
function spline(
    currents::AbstractVector,
    Norder::Integer,
    Nknots::Integer,
    coeffs::AbstractVector,
)
    @assert length(coeffs) == (1 + Norder + Nknots) + Nknots
    termcoeffs = @view coeffs[1:(1+Norder+Nknots)]
    knotcoeffs = @view coeffs[(1+Norder+Nknots).+(1:Nknots)]
    temp = similar(currents, length(currents), length(termcoeffs))
    for (i, o) in enumerate(0:Norder)
        @. temp[:, i] = termcoeffs[i] * currents^o
    end
    for (i, z) in enumerate(knotcoeffs)
        @. temp[:, i+Norder+1] = h(currents, z, Norder) * termcoeffs[i+Norder+1]
    end
    sum(temp', dims = 1)[:]
end

"""
    splinefit(
		Ncurrents::Integer,
		Norder::Integer,
		Nknot::Integer,
		currents::AbstractMatrix,
		coeffs::AbstractVector,
	)

Predict array power using splines of order `Norder` with `Nknot` many
knots operating on `Ncurrents` number of device currents.
"""
function splinefit(
    Ncurrents::Integer,
    Norder::Integer,
    Nknot::Integer,
    currents::AbstractMatrix,
    coeffs::AbstractVector,
)
    Nshiftcoeffs = (Ncurrents - 1)
    Nsplinecoeffs = ((1 + Norder + Nknot) + Nknot)
    @assert length(coeffs) == (Ncurrents * Nshiftcoeffs) + Ncurrents * Nsplinecoeffs
    shiftcoeffs = @view coeffs[1:(Ncurrents*Nshiftcoeffs)]
    # and the spline coefficients
    splinecoeffs = @view coeffs[(Ncurrents*Nshiftcoeffs+1):end]
    shiftedcurrents = similar(currents)
    @views for i = 1:Ncurrents
        shiftedcurrents[i, :] .= (currents[
            1:Ncurrents.!==i,
            :,
        ].*shiftcoeffs[(1:Ncurrents-1).+(i-1)*Nshiftcoeffs])[:]
    end
    powers = similar(currents)
    @views for i = 1:Ncurrents
        powers[i, :] .=
            relu.(
                spline(
                    shiftedcurrents[i, :],
                    Norder,
                    Nknot,
                    splinecoeffs[(1:Nsplinecoeffs).+(i-1)*Nsplinecoeffs],
                ),
            )
    end
    vec(sum(powers, dims = 1))
end


"""
    analyticpower_analysis(
		currents::AbstractMatrix,
		power::AbstractVector;
		order::Integer = 1,
		overestimationloss::Number = 0,
		knots::Integer = 3,
		splines::Bool = false
		method = Optim.NelderMead(),
		iterations::Integer = 1000,
		time_limit::Integer = 60,
		initrand::Bool = true,
		filtercoherent::Bool = false,
		filterthreshold::Bool = false,
		threshold::Number = 3,
	)->(power, analyticpower, excesspower, powerfun)

Predict the uncoupled array power and power enhancement using an
analytical fit to the data. Will use a thresholded polynomial fit to
each device whose threshold shifts as a function of other currents.
Polynomial order is defined by `order`. `overestimationloss` punishes
estimated power that exceeds measured power. Can optionally use splines
if `splines` is set `true` with `knots` many knots. Will return not only
vectors of the power values, but a current-power modeling function `powerfun`.
"""
function analyticpower_analysis(
    currents::AbstractMatrix,
    power::AbstractVector;
    order::Integer = 1,
    overestimationloss::Number = 0,
    knots::Integer = 3,
    splines::Bool = false,
    method = Optim.NelderMead(),
    iterations::Integer = 1000,
    time_limit::Integer = 60,
    initrand::Bool = true,
    filtercoherent::Bool = false,
    filterthreshold::Bool = false,
    threshold::Number = 3,
)
    @assert size(currents, 2) == length(power)
    @assert order > 0

    # Remove above multiple threshold data if desired
    ftmask = vec(count(>(threshold), currents, dims = 1) .<= 1)
    currents0, power0 = copy(currents), copy(power)
    currents, power = currents[:, ftmask], power[ftmask]

    Ncurrents = size(currents, 1)
    Ncoeffs =
        splines ?
        Ncurrents * (order + 1) + 2 * knots * Ncurrents + Ncurrents * (Ncurrents - 1) :
        Ncurrents * (Ncurrents - 1) + Ncurrents * (order + 1)
    # Create the modeling function
    model(coeffs::AbstractVector) =
        splines ? splinefit(Ncurrents, order, knots, currents, coeffs) :
        polyfit(Ncurrents, order, currents, coeffs)
    # Optimize the coefficients
    loss(coeffs::AbstractVector) =
        mse(model(coeffs), power) + overestimationloss * mean(relu(model(coeffs) - power))
    coeffs = Optim.minimizer(
        Optim.optimize(
            loss,
            initrand ? rand(Ncoeffs) : zeros(Ncoeffs),
            method = method,
            iterations = iterations,
            time_limit = time_limit,
        ),
    )

    #TODO: Test and improve this section (expose power enhancement threshold?)
    # If desired...
    if filtercoherent
        # Obtain the fitted power
        powers_fitted = model(coeffs)
        # Predict the power enhancement...
        power_enhancement = @. power - powers_fitted
        # And filter out points that have "significant" power enhancement...
        mask = power_enhancement .< maximum(power_enhancement) / 2
        currents, power = currents[:, mask], power[mask]

        # And re-train the model (hopefully) excluding the coherent points from the training set
        # Create the modeling function
        model(coeffs::AbstractVector) =
            splines ? splinefit(Ncurrents, order, knots, currents, coeffs) :
            polyfit(Ncurrents, order, currents, coeffs)
        # Optimize the coefficients
        loss(coeffs::AbstractVector) =
            mse(model(coeffs), power) +
            overestimationloss * mean(relu(model(coeffs) - power))
        coeffs = Optim.minimizer(
            Optim.optimize(
                loss,
                initrand ? rand(Ncoeffs) : zeros(Ncoeffs),
                method = method,
                iterations = iterations,
                time_limit = time_limit,
            ),
        )
    end

    # Create function that calculates predicted power
    powerfun(i) =
        splines ? splinefit(Ncurrents, order, knots, i, coeffs) :
        polyfit(Ncurrents, order, i, coeffs)
    # Obtain the fitted power
    analyticpower = powerfun(currents0)
    excesspower = power0 - analyticpower
    power0, analyticpower, excesspower, powerfun
end
