module VCSELArrayAnalysis

include("utils.jl")
include("farfield_beamsteering.jl")
include("farfield_fft.jl")
include("farfield_visibility.jl")
include("linear_modeling.jl")
include("spectral_predictor.jl")
#TODO: Spectrum peak-count, peak-spacing
include("voltage.jl")
include("power.jl")

export beamsteering_analysis,
    farfieldfftpeak_analysis,
    powerridge_analysis,
    naivepower_analysis,
    voltage_analysis,
    farfieldvisibility_analysis,
    analyticpower_analysis

#TODO: Add tests
#TODO: Try to get N-D support for far-fields, LIV stuff, etc

end
