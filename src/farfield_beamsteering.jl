#TODO: N-D versions

"""
    beamsteering_angle(angles, farfield)

Find the far-field beam-steering angle, defined as the angle of the
maximal far-field intensity.
"""
function beamsteering_angle(angles::AbstractVector, farfield::AbstractVector)
    @assert length(angles) == length(farfield)
    angles[findmax(farfield)[2]]
end
function beamsteering_angle(angles::AbstractVector, farfield::AbstractMatrix)
    @assert length(angles) == size(farfield, 1)
    angles[getindex.(findmax(farfield, dims = 1)[2], 1)]
end

# https://www.electronics-notes.com/articles/antennas-propagation/smart-adaptive-antennas/beamforming-beamsteering-antenna-basics.php
"""
    phasedifference(spacing, lambda, theta)

Calculate the inter-cavity phase difference from the far-field
beam-steering angle. Assumes that antenna beam-steering theory is applicable.

# Arguments
- `spacing`: the spacing between VCSEL elements.
- `wavelength`: the emission wavelength.
- `angle`: the far-field beam-steering angle (in radians).
"""
phasedifference(spacing::Number, wavelength::Number, angle::Number) =
    2 * pi * spacing * sin(angle) / wavelength
phasedifference(spacing::Number, wavelength::Number, angle::AbstractArray) =
    @. 2 * pi * spacing * sin(angle) / wavelength

"""
    phasenorm(x)

Normalize the (angle in radians) to be between `-pi` and `pi`.
"""
phasenorm(x::Number) = mod2pi(x) > pi ? mod2pi(x) - 2 * pi : mod2pi(x)
phasenorm(x::AbstractArray) = (t = mod2pi.(x); @. t - 2 * pi * (t > pi))

"""
	beamsteering_analysis(
		angles,
		farfields,
		spacing,
		wavelength,
	)->(angle, phase)

Determine the beam-steering angle and the inter-cavity phase difference
from far-field measurements.

# Arguments
- `angles`: the list of angle values (in radians) for the far-field.
- `farfields`: the array of 1 or more 1D far-field measurements (1D or 2D array).
- `spacing`: the spacing between VCSEL elements.
- `wavelength`: the emission wavelength.
- `angle`: the far-field beam-steering angle (in radians).
"""
function beamsteering_analysis(
    angles::AbstractVector,
    farfields::AbstractMatrix,
    spacing::Number,
    wavelength::Number,
)
    @assert size(farfields, 1) == length(angles)
    if any(abs.(angles) .> pi / 2)
        @warn "`angles` should be in radians."
    end
    # Determine beam-steering angle for each far-field
    beamsteeringangles = vec(beamsteering_angle(angles, farfields))
    # Determine corresponding inter-cavity phase difference
    phases = phasedifference(spacing, wavelength, beamsteeringangles)
    beamsteeringangles, phasenorm(phases)
end
function beamsteering_analysis(
    angles::AbstractVector,
    farfields::AbstractVector,
    spacing::Number,
    wavelength::Number,
)
    @assert length(farfields) == length(angles)
    if any(abs.(angles) .> pi / 2)
        @warn "`angles` should be in radians."
    end
    # Determine beam-steering angle for each far-field
    beamsteeringangles = beamsteering_angle(angles, farfields)
    # Determine corresponding inter-cavity phase difference
    phases = phasedifference(spacing, wavelength, beamsteeringangles)
    beamsteeringangles, phasenorm(phases)
end
