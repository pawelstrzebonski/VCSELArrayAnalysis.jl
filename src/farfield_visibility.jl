import FindPeaks

"""
    farfieldvisibility_analysis(
		farfield;
		N = 10,
		w = 10,
		height = 0.1,
		distance = 0.1,
		ave = false,
		env = false,
	)

Analyze the far-field coherence using the visibility metric. Calculates
the far-field visibility as a ratio between the difference in peak and
minima intensities to their sum.

# Arguments
- `farfield`: a vector for a single 1D far-field measurement or a matrix of many.
- `N = 10`: padding for the Gaussian smoothing.
- `w = 10`: the width of Gaussian smoothing.
- `height = 0.1`: the minimal fraction of peak intensity for a peak to be recognized.
- `distance = 0.1`: the minimal fraction of far-field image width between peaks.
- `ave = false`: whether to average the maxima/minima in intensity (or else just use extreme values).
- `env = false`: whether to try to remove envelope before analysis.
"""
function farfieldvisibility_analysis(
    farfield::AbstractVector;
    N::Integer = 10,
    w::Number = 10,
    height::Number = 0.1,
    distance::Number = 0.1,
    ave::Bool = false,
    env::Bool = false,
    Nenv::Integer = 10,
)
    # Calculate and remove an envelope using max-filter/gaussian-smooth method
    if env
        envelope = maxfilter(farfield, N)
        envelope = gaussiansmooth(envelope, Nenv, N = Nenv)
        farfield ./= envelope
    end
    # Gaussian smoothing to reduce noise
    farfield = gaussiansmooth(farfield, w, N = N)
    # Find peaks above 10% of peak intensity
    maxima = FindPeaks.findpeaks(
        farfield,
        height = maximum(farfield) * height,
        distance = round(Int, length(farfield) * distance),
    )
    # Find all minima
    minima =
        FindPeaks.findpeaks(-farfield, distance = round(Int, length(farfield) * distance))
    # Select only minima between the peaks
    minima = minima[minimum(maxima).<minima.<maximum(maxima)]
    # If there are no minima, then zero visibility by definition
    if iszero(length(minima))
        return 0
    end
    # Average peak and valley intensity or else most extreme values
    Imax = ave ? mean(farfield[maxima]) : maximum(farfield[maxima])
    Imin = ave ? mean(farfield[minima]) : minimum(farfield[minima])
    # Visibility
    (Imax - Imin) / (Imax + Imin)
end
function farfieldvisibility_analysis(
    farfields::AbstractMatrix;
    N::Integer = 10,
    w::Number = 10,
    height::Number = 0.1,
    distance::Number = 0.1,
    ave::Bool = false,
)
    [
        farfieldvisibility_analysis(
            farfield,
            N = N,
            w = w,
            height = height,
            distance = distance,
            ave = ave,
        ) for farfield in eachslice(farfields, dims = 2)
    ]
end
